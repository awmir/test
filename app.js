// libraries:
const express = require('express');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const cookieEncrypter = require('cookie-encrypter');
const {cpus} = require('os');
const helmet = require("helmet");

// environment variable
require('dotenv').config({path: `${__dirname}/.env`});


// *** thread pool size (this environment variable must use in production version an JUST in Master Process) ***
process.env.UV_THREADPOOL_SIZE = String(cpus().length);
process.env.MAIN_PATH = process.cwd();

// config
const config = require('./config');

// set global config
global.config = config;

mongoose.connect(
    `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/test`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });


mongoose.connection.once('open', () => {
    console.log(`Database is connected on ${mongoose.connection.host}:${mongoose.connection.port}`)
})


mongoose.connection.on('error', (e) => {
    console.log(e)
})


// routes :
const mainRouter = require('./routes/mainRouter');
const path = require("path");

// webserver application instance
const app = express();

// security middleware configs
app.use(helmet());
// set the port in main of application
app.set('port', config.port);

// serve static file


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser(process.env.COOKIE_SECRET));
app.use(cookieEncrypter(process.env.COOKIE_SECRET));

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        return res.status(400).json({
            success: false,
            message: "BAD JSON"
        })
    }
    return next()
});

//  main routes
app.use('/', mainRouter);

app.listen(config.port, async () => {
    console.log(`process is run successfully on port:${config.port}`);
});

