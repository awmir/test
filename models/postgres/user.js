const {join} = require('path');
const {sequelize, DataTypes} = require(join(config.path.models.postgres.main, 'root'));
const bcrypt = require("bcryptjs");

const User = sequelize.define("user", {
    name: {
        type: DataTypes.STRING(64),
        allowNull: false,
        validate: {
            len: [1, 30]
        }
    },
    age: {
        type: DataTypes.INTEGER,
        allowNull: false,
        validate: {
            max: 80
        }
    },
    mobile: {
        type: DataTypes.STRING(64),
        allowNull: false,
        validate: {
            len: [11, 11]
        }
    },
    password: {
        type: DataTypes.STRING(64),
        allowNull: false,
    },
    isActive: {
        type: DataTypes.Boolean,
        allowNull: false,
        defaultValue: true
    }
});

User.addHook('beforeCreate', async (user) => {
    const salt = await bcrypt.genSalt()
    user.password = await bcrypt.hash(this.password, salt)
});

(async () => {
    await sequelize.sync({force: true});
})();

module.exports = User;