const {Sequelize, DataTypes} = require('sequelize');

const sequelize = new Sequelize('postgres', 'postgres', '12345678', {
    dialect: 'postgres',
    logging: false
});

module.exports = {sequelize, DataTypes};