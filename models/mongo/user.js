const {Schema, model} = require('mongoose');
const bcrypt = require('bcryptjs');


const userSchema = new Schema({
    name: {type: String, required: true},
    age: {type: Number, required: true},
    mobile: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    isActive: {type: Boolean, required: true, default: true}
}, {timestamps: true});


userSchema.pre('save', async function (next) {
    if (this.isModified('password')) {
        const salt = await bcrypt.genSalt()
        this.password = await bcrypt.hash(this.password, salt)
    } else {
        return next();
    }
});

userSchema.pre(['updateOne', 'findOneAndUpdate'], async function (next) {
    if (this._update['password']) {
        const salt = await bcrypt.genSalt()
        this._update.password = await bcrypt.hash(this._update.password, salt);
    } else {
        return next();
    }
});

const customerModel = model('user', userSchema, 'users');


module.exports = customerModel

