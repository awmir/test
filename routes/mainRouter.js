const router = require('express').Router();
const jwt = require('jsonwebtoken');


// routes :
const authRouter = require('./auth/authRouter')
const userRouter = require('./user/profile')

// include api routes :
router.use('/auth', authRouter);

router.use('/user', async (req, res, next) => {

    try {

        const {access_token: accessToken} = req.headers;

        if (!accessToken) {
            return res.status(401).json({
                success: false,
                message: "توکن در درخواست ارایه شده موجود نمی باشد"
            })
        }

        try {
            await jwt.verify(accessToken, process.env.SECRET_KEY);
        } catch (e) {
            return res.status(401).json({
                success: false,
                message: "این توکن معتبر نمی باشد"
            })
        }

        return next();

    } catch (e) {
        console.log(e)
    }


}, userRouter);


module.exports = router;

