const router = require('express').Router();
const {join} = require('path');
const {validationResult} = require('express-validator');

//models
const userModel = require(join(config.path.models.mongo.main, 'user'));


//modules
const {checkRequestFields} = require(join(config.path.modules.utils, 'checkFields'));
const {refactorErrors} = require(join(config.path.modules.utils, 'validationRefactored'));

//validation
const {register} = require('./validations');

router.post('/',
    register,
    async (req, res) => {
        try {

            const validFields = ["name", "age", "mobile", "password", "rePassword"];

            const isStructureValid = checkRequestFields(req.body, validFields, validFields.length, true);
            if (!isStructureValid) {
                return res.status(400).json({
                    success: false,
                    message: 'فیلد های ارسالی معتبر نیستند'
                });
            }

            const validationRes = validationResult(req);
            if (!validationRes.isEmpty()) {
                const invalidFields = refactorErrors(validationRes.array());
                return res.status(403).json({
                    success: false,
                    invalidFields,
                    message: "درخواست نامعتبر"
                });
            }

            const {name, age, mobile, password, rePassword} = req.body;

            if (password !== rePassword) {
                return res.status(403).json({
                    success: false,
                    message: "لطفا تکرار رمز عبور خود را با دقت وارد کنید"
                })
            }

            if (age > 80) {
                return res.status(403).json({
                    success: false,
                    message: "سن انتخابی باید کمتر از 80 سال باشد"
                })
            }

            const user = await userModel.findOne({mobile});


            if (user) {
                return res.status(400).json({
                    success: false,
                    message: "یان کاربر قبلا ثبت نام کرده است"
                });
            }

            await userModel.create({
                name,
                age,
                mobile,
                password
            });

            return res.status(200).json({
                success: true,
                message: "ثبت نام شما با موفقیت انجام شد"
            })


        } catch (e) {
            console.log(e)
            return res.status(500).json({
                success: false,
                message: "Internal Server Error"
            })
        }
    });


module.exports = router;