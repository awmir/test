const router = require('express').Router();
const {join} = require('path');
const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


//models
const userModel = require(join(config.path.models.mongo.main, 'user'));


//modules
const {checkRequestFields} = require(join(config.path.modules.utils, 'checkFields'));
const {refactorErrors} = require(join(config.path.modules.utils, 'validationRefactored'));

//validation
const {login} = require('./validations');

router.post('/',
    login,
    async (req, res) => {
        try {

            const validFields = ["mobile", "password"];

            const isStructureValid = checkRequestFields(req.body, validFields, validFields.length, true);
            if (!isStructureValid) {
                return res.status(400).json({
                    success: false,
                    message: 'فیلد های ارسالی معتبر نیستند'
                });
            }

            const validationRes = validationResult(req);
            if (!validationRes.isEmpty()) {
                const invalidFields = refactorErrors(validationRes.array());
                return res.status(403).json({
                    success: false,
                    invalidFields,
                    message: "درخواست نامعتبر"
                });
            }

            const {mobile, password} = req.body;

            const user = await userModel.findOne({mobile});


            if (!user) {
                return res.status(400).json({
                    success: false,
                    message: " کاربری با این مشخصات پیدا نشد"
                });
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (!isMatch) {
                return res.status(400).json({
                    success: false,
                    message: "مشخصات وارد شده صحیح نیست"
                });
            }

            const accessToken = await jwt.sign({email: user.mobile}, process.env.SECRET_KEY, {expiresIn: 180 * 1000});

            return res.status(200).json({
                success: true,
                accessToken,
                message: " با موفقیت وارد شدید"
            })


        } catch (e) {
            console.log(e)
            return res.status(500).json({
                success: false,
                message: "Internal Server Error"
            })
        }
    });


module.exports = router;