const {body, query} = require('express-validator');

module.exports = {
    register: [
        body('name')
            .notEmpty().withMessage('وارد کردن نام اجباری است')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست')
            .isLength({min: 1, max: 30}).withMessage('تعداد کاراکتر های وارد شده مجاز نیست'),

        body('age')
            .notEmpty().withMessage('وارد کردن سن اجباری است')
            .isNumeric().withMessage('اطلاعات وارد شده مجاز نیست')
            .custom((value) => {
                return value < 80
            }).withMessage('سن وارد شده بیشتر از حد مجاز است'),


        body('mobile')
            .notEmpty().withMessage('وارد کردن شماره موبایل اجباری است')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست')
            .isLength({min: 11, max: 11}).withMessage('تعداد کاراکتر های وارد شده مجاز نیست'),


        body('password')
            .notEmpty().withMessage('وارد کردن رمز عبور اجباری است')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست')
            .isLength({min: 8, max: 100}).withMessage('رمز انتخابی باید حداقل 8 کاراکتر باشد')
            .matches(/[A-Z]/).withMessage('رمز انتخابی باید شامل حروف بزرگ شود')
            .matches(/[a-z]/).withMessage('رمز انتخابی باید شامل حروف کوچک شود')
            .matches(/[0-9]/).withMessage('رمز انتخابی باید شامل اعداد شود'),


        body('rePassword')
            .notEmpty().withMessage('وارد کردن تکرار رمز عبور اجباری است')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست'),
    ],
    login: [
        body('mobile')
            .notEmpty().withMessage('وارد کردن شماره موبایل اجباری است')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست')
            .isLength({min: 11, max: 11}).withMessage('تعداد کاراکتر های وارد شده مجاز نیست'),

        body('password')
            .notEmpty().withMessage('وارد کردن رمز عبور اجباری است')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست')
    ]

}
