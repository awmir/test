const {body, query, param} = require('express-validator');

module.exports = {
    profile: [
        param('userId')
            .notEmpty().withMessage('این فیلد نباید خالی باشد')
            .isMongoId().withMessage('اطلاعات وارد شده صحیح نیست')
    ],
    update: [
        param('userId')
            .notEmpty().withMessage('این فیلد نباید خالی باشد')
            .isMongoId().withMessage('اطلاعات وارد شده صحیح نیست'),
        body('name')
            .isString().withMessage('اطلاعات وارد شده مجاز نیست')
            .isLength({min: 1, max: 30}).withMessage('تعداد کاراکتر های وارد شده مجاز نیست'),

        body('age')
            .isNumeric().withMessage('اطلاعات وارد شده مجاز نیست')
            .custom((value) => {
                return value < 80
            }).withMessage('سن وارد شده بیشتر از حد مجاز است'),
    ],
    deActive: [
        param('userId')
            .notEmpty().withMessage('این فیلد نباید خالی باشد')
            .isMongoId().withMessage('اطلاعات وارد شده صحیح نیست')
    ]

}
