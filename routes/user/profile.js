const router = require('express').Router();
const {join} = require('path');
const {validationResult} = require('express-validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


//models
const userModel = require(join(config.path.models.mongo.main, 'user'));


//modules
const {checkRequestFields} = require(join(config.path.modules.utils, 'checkFields'));
const {refactorErrors} = require(join(config.path.modules.utils, 'validationRefactored'));
const {removeEmptyFieldsFromObject} = require(join(config.path.modules.utils, 'helpers'));


//validation
const {profile, update, deActive} = require('./validations');

router.get('/:userId',
    profile,
    async (req, res) => {
        try {

            const validationRes = validationResult(req);
            if (!validationRes.isEmpty()) {
                const invalidFields = refactorErrors(validationRes.array());
                return res.status(403).json({
                    success: false,
                    invalidFields,
                    message: "درخواست نامعتبر"
                });
            }

            const {userId} = req.params;

            const user = await userModel.findOne({
                _id: userId,
                isActive: true
            }).select({
                __v: 0,
                _id: 0
            });

            if (!user) {
                return res.status(400).json({
                    success: false,
                    message: " کاربری با این مشخصات پیدا نشد"
                });
            }

            return res.status(200).json({
                success: true,
                data: user,
                message: "درخواست شما با موفقیت انجام شد"
            })


        } catch (e) {
            console.log(e)
            return res.status(500).json({
                success: false,
                message: "Internal Server Error"
            })
        }
    });

router.put('/:userId/update', update, async (req, res) => {
    try {

        const validFields = ["name", "age"];

        const isStructureValid = checkRequestFields(req.body, validFields, validFields.length, false);
        if (!isStructureValid) {
            return res.status(400).json({
                success: false,
                message: 'فیلد های ارسالی معتبر نیستند'
            });
        }

        const validationRes = validationResult(req);
        if (!validationRes.isEmpty()) {
            const invalidFields = refactorErrors(validationRes.array());
            return res.status(403).json({
                success: false,
                invalidFields,
                message: "درخواست نامعتبر"
            });
        }

        const {userId} = req.params;
        const {name, age} = req.body;

        const data = removeEmptyFieldsFromObject({
            name,
            age
        });

        if (!Object.entries(data).length) {
            return res.status(200).json({
                success: false,
                message: "لطفا مقادیری را برای به روز رسانی وارد کنید"
            })
        }

        const user = await userModel.findOne({
            _id: userId,
            isActive: true
        }).select({
            __v: 0,
            _id: 0
        });

        if (!user) {
            return res.status(400).json({
                success: false,
                message: "و کاربری با این مشخصات پیدا نشد"
            });
        }

        if (age > 80) {
            return res.status(403).json({
                success: false,
                message: "سن انتخابی باید کمتر از 80 سال باشد"
            })
        }


        await userModel.findOneAndUpdate({
            mobile: user.mobile
        }, {
            ...data
        });

        return res.status(200).json({
            success: true,
            message: "اطلاعات با موفقیت آپدیت شد"
        })

    } catch (e) {
        console.log(e)
        return res.status(500).json({
            success: false,
            message: "Internal Server Error"
        })
    }
});


router.put('/:userId/deActive', deActive, async (req, res) => {
    try {

        const validationRes = validationResult(req);
        if (!validationRes.isEmpty()) {
            const invalidFields = refactorErrors(validationRes.array());
            return res.status(403).json({
                success: false,
                invalidFields,
                message: "درخواست نامعتبر"
            });
        }

        const {userId} = req.params;

        const user = await userModel.findOne({
            _id: userId,
            isActive: true
        }).select({
            __v: 0,
            _id: 0
        });

        if (!user) {
            return res.status(400).json({
                success: false,
                message: " کاربری با این مشخصات پیدا نشد"
            });
        }

        await userModel.findOneAndUpdate({
            mobile: user.mobile
        }, {
            isActive: false
        });

        return res.status(200).json({
            success: true,
            message: "اکانت شما با موفقیت غیر فعال شد"
        })

    } catch (e) {
        console.log(e)
        return res.status(500).json({
            success: false,
            message: "Internal Server Error"
        })
    }
});


module.exports = router;