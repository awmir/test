module.exports = {
    refactorErrors(errorList = []) {
        const errors = [];
        errorList.forEach(errorObj => {
            let status = true;
            for (let error of errors) {
                if (error.param === errorObj.param && error.location === errorObj.location) {
                    status = false
                    break
                }
            }
            if (status) errors.push(errorObj);
        })
        return errors;
    },
}