// remove undefined fields from an object
const removeEmptyFieldsFromObject = (data) => {
    for (let field in data) {
        // if the value is undefined, remove it
        if (data[field] === undefined) {
            delete data[field];

            // todo what is symbol
        } else if (field === 'symbol') {
            data[field] = data[field].toUpperCase();
        }
    }

    return data;
}

module.exports = {removeEmptyFieldsFromObject};