module.exports = {

    checkRequestFields(requestObj, allowedFields, validLength, required = true) {

        const requestObjKeys = Object.keys(requestObj);

        // check if length of request (obj) is require to be equal to a number or if not and after check the number of length validation of request
        if (required === true && (requestObjKeys.length !== validLength) || required === false && (requestObjKeys.length > validLength)) {
            return false
        }

        // request body must  include all valid request body properties
        if (required) {

            for (let propertyName of allowedFields) {
                if (!requestObjKeys.includes(propertyName)) {
                    return false
                }
            }

        } else {

            for (let propertyName of requestObjKeys) {
                if (!allowedFields.includes(propertyName)) {
                    return false
                }
            }

        }
        return true
    }
}