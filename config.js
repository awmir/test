const {join} = require('path');


module.exports = {
    port: 4001,

    path: {
        main: process.cwd(),
        modules: {
            utils: join(process.cwd(), 'modules', 'utils')
        },
        models: {
            mongo: {
                main: join(process.cwd(), 'models', 'mongo')
            },
            postgres: {
                main: join(process.cwd(), 'models', 'postgres')
            }
        }
    }
};


